use std::{env::args, io::stdin, ops::ControlFlow, path::PathBuf};

use lofty::{ItemKey, ItemValue, Probe, Tag, TagExt, TaggedFile, TaggedFileExt};

static USAGE: &str = "\
Auded: Edit audio file metadata

Usage: auded [options] [file...]

Option:
  -h, --help          Display this help
  +<cmd>              Execute <cmd> on [file]

Commands:
  w                   Write new metadata to file
  pa                  Print all the tags
  p                   Print the common tags for file
  p/<tag>             Print the value of the tag
  f/<tag>/<value>     Skip the file if tag isn't value
  s/<tag>/<value>     Set a tag
  d/<tag>             Delete a tag

Interactive Commands:
When launched without any +<cmd> opitons.
  o <file>            Open <file>
  q                   Quit the editor
";

fn main() {
    let args = args().skip(1).collect::<Vec<_>>();

    if args.iter().any(|a| a == "--help" || a == "-h") {
        print!("{USAGE}");
        return;
    }

    let mut app = App::default();

    let mut paths = args
        .iter()
        .map(PathBuf::from)
        .filter(|p| p.is_file())
        .rev()
        .collect::<Vec<_>>();

    let cmds = args
        .iter()
        .filter(|a| a.starts_with('+'))
        .collect::<Vec<_>>();

    if cmds.is_empty() {
        paths.pop().map(|p| app.open(p));
        for cmd in stdin().lines().map(|c| c.unwrap()) {
            if app.run(parse(&cmd)).is_break() {
                break;
            }
        }
        return;
    }

    paths.reverse();

    'path: for path in paths {
        println!("\n\x1B[1m{}\x1B[0m", path.display());
        if !app.open(path) {
            continue;
        }
        for cmd in cmds.iter().filter_map(|s| s.strip_prefix('+')) {
            println!("\x1B[1m+{cmd}\x1B[0m");
            if app.run(parse(cmd)).is_break() {
                continue 'path;
            };
        }
    }
}

#[derive(Default)]
struct App {
    path: Option<PathBuf>,
    temp_tag: Option<Tag>,
    tagged_file: Option<TaggedFile>,
}

impl App {
    fn tag(&self) -> Option<&Tag> {
        self.tagged_file.as_ref().and_then(TaggedFile::primary_tag)
    }

    fn tag_mut(&mut self) -> Option<&mut Tag> {
        self.tagged_file
            .as_mut()
            .and_then(TaggedFile::primary_tag_mut)
    }

    fn write(&mut self) {
        if let Some(file) = self.tagged_file.as_mut() {
            if let Some(tag) = file.primary_tag() {
                if let Err(e) = tag.save_to_path(self.path.as_ref().unwrap()) {
                    eprintln!("failed to write tags to file: {e}")
                }
            } else if let Some(tag) = self.temp_tag.take() {
                file.insert_tag(tag);
            }
        }
    }

    fn open(&mut self, path: PathBuf) -> bool {
        match Probe::open(&path).and_then(Probe::read) {
            Ok(f) => self.tagged_file = Some(f),
            Err(_e) => {
                // eprintln!("failed to open: {_e}");
                return false;
            }
        }

        self.path = Some(path);
        true
    }

    fn run(&mut self, cmd: Command) -> ControlFlow<()> {
        match cmd {
            Command::Open(p) if p.is_file() => {
                if !self.open(p) {
                    eprintln!("Failed to open file")
                }
            }
            Command::Open(path) => eprintln!("{} not a file", path.display()),
            Command::Quit => return ControlFlow::Break(()),
            Command::Unknown => eprintln!("Unknown command"),
            Command::NoParam => eprintln!("Bad parameter"),
            Command::None => (),
            _ if self.tagged_file.is_none() => eprintln!("No file open"),
            Command::Query(ref key) => {
                if let Some(tag) = self.tag() {
                    println!("{}", tag.get_string(key).unwrap_or("<empty>"))
                } else {
                    println!("No tags found");
                }
            }
            Command::QueryAll(unknown) => {
                if let Some(tags) = self.tag().map(|t| t.items()) {
                    for (key, val) in tags.map(|ti| (ti.key(), ti.value())) {
                        if !unknown && matches!(key, ItemKey::Unknown(_)) {
                            continue;
                        }
                        match (key, val) {
                            (ItemKey::Unknown(key), ItemValue::Text(val)) => {
                                println!("Unknown({key}): {val}");
                            }
                            (_, ItemValue::Text(val)) => println!("{key:?}: {val}"),
                            _ => println!("failed to parse value"),
                        }
                    }
                } else {
                    println!("No tags found");
                }
            }
            Command::Filter(key, val) => {
                if self.tag().and_then(|t| t.get_string(&key)) != Some(&val) {
                    println!(
                        "skipping {}",
                        self.path
                            .as_ref()
                            .unwrap()
                            .file_name()
                            .unwrap()
                            .to_string_lossy()
                    );
                    return ControlFlow::Break(());
                }
            }
            Command::Set(key, val) => {
                if let Some(tag) = self.tag_mut() {
                    tag.insert_text(key, val);
                } else {
                    self.temp_tag = Some(Tag::new(
                        self.tagged_file.as_ref().unwrap().primary_tag_type(),
                    ))
                }
            }
            Command::Delete(ref key) => self.tag_mut().unwrap().remove_key(key),
            Command::Write => self.write(),
        }

        ControlFlow::Continue(())
    }
}

enum Command {
    Open(PathBuf),
    Query(ItemKey),
    QueryAll(bool),
    Filter(ItemKey, String),
    Set(ItemKey, String),
    Delete(ItemKey),
    Quit,
    NoParam,
    Unknown,
    Write,
    None,
}

fn parse(cmd_str: &str) -> Command {
    let (cmd, arg) = cmd_str.split_once(' ').unwrap_or((cmd_str, ""));
    let res = match cmd {
        "q" | "quit" => Command::Quit,
        "o" | "open" => match arg {
            "" => Command::NoParam,
            s => Command::Open(
                s.replace("~", std::env::var("HOME").as_deref().expect("HOME Not set"))
                    .into(),
            ),
        },
        "pa" | "printall" => Command::QueryAll(true),
        "w" | "write" => Command::Write,
        "" => Command::None,
        _ => Command::Unknown,
    };

    if !matches!(res, Command::Unknown) {
        return res;
    }

    let mut split = cmd_str.split('/');
    match split.next().unwrap() {
        "p" | "print" => match split.next().map(tag_name) {
            Some(key) => Command::Query(key),
            None => Command::QueryAll(false),
        },
        "f" | "filter" => {
            let Some(key) = split.next().map(tag_name) else {return Command::NoParam};
            let Some(val) = split.next() else {return Command::NoParam};
            Command::Filter(key, val.to_owned())
        }
        "s" | "set" => {
            let Some(key) = split.next().map(tag_name) else {return Command::NoParam};
            let Some(val) = split.next() else {return Command::NoParam};
            Command::Set(key, val.to_owned())
        }
        "d" | "del" | "delete" => match split.next().map(tag_name) {
            Some(param) => Command::Delete(param),
            _ => Command::NoParam,
        },
        _ => Command::Unknown,
    }
}

fn tag_name(tag: &str) -> ItemKey {
    match tag {
        "title" => ItemKey::TrackTitle,
        "artist" => ItemKey::TrackArtist,
        "album" => ItemKey::AlbumTitle,
        "album artist" => ItemKey::AlbumArtist,
        "year" => ItemKey::Year,
        "date" => ItemKey::RecordingDate,
        "track" => ItemKey::TrackNumber,
        "genre" => ItemKey::Genre,
        "comment" => ItemKey::Comment,
        _ => ItemKey::Unknown(tag.to_owned()),
    }
}
