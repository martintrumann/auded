Auded - Audio Editor
---
This is a simple audio metadata editor using lofty.

## Usage
Two modes are supported:
- Interactive
- Batch

interactive mode allows you to write commands to stdin.
batch mode uses `+` to parse arguments commands 

Commands are simple:
- Quit(q) - Leave the application.
- Open(o) - Open another file.
- Set(s) - Set a tag to value.
- Filter(f) - skip a file in batch mode if tag isn't set to value.
- Delete(del, d) - delete a tag.
- Print(p) - print all the known tags or specify a single tag.
- PrintAll(pa) - print the unknown tags as well.
- Write(w) - write the changes to the file.

recognized tags are
- title
- artist
- album
- album\_artist
- date
- track
- genre

the rest are considered unknown
